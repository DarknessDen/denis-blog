import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {PostsService} from "../../shared/posts.service";
import {switchMap} from "rxjs/operators";
import {Post} from "../../shared/interfaces";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {AlertService} from "../shared/services/alert.service";

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss']
})
export class EditPageComponent implements OnInit, OnDestroy {

  editPostForm: FormGroup;
  post: Post;
  submitted = false;

  updateSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private postsService: PostsService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    // Subscribing on current route to get params (unsubscription is authomatic)
    this.route.params.pipe(
      switchMap((params: Params) => {
        return this.postsService.getById(params['id'])
      })).subscribe((post: Post) => {
        this.post = post;
        this.editPostForm = new FormGroup({
          title: new FormControl(post.title, Validators.required),
          text: new FormControl(post.text, Validators.required)
        })
      })
  }

  submit() {
    if (this.editPostForm.invalid) {
      return
    }

    this.submitted = true;

    this.updateSub = this.postsService.update({
      // using spread operator to get all non-changable post data
      ...this.post,
      //rewriting changed post data
      text: this.editPostForm.value.text,
      title: this.editPostForm.value.title
    }).subscribe(() => {
      this.submitted = false;
      this.alertService.success('Post was changed');
    })
  }

  ngOnDestroy() {
    if (this.updateSub) {
      this.updateSub.unsubscribe();
    }
  }
}
